# -*- coding: utf-8 -*-
"""
"""
import bpy
from bpy.props import (
    BoolProperty,
    IntProperty,
    FloatProperty,
    StringProperty,
    FloatVectorProperty,
    CollectionProperty
)

class MEASURE_SPEED_measure_settings(bpy.types.PropertyGroup):
    """ """
    ob_name : StringProperty(name="Object name", description="Object name", default="")
    ob_pos : FloatVectorProperty(name="Object position", description="Object last processed position", size=3, default=(0.0, 0.0, 0.0), subtype='TRANSLATION')
    ob_delta : FloatProperty(name="Object position delta", description="Object position delta", default=0.0)

bpy.utils.register_class(MEASURE_SPEED_measure_settings)

class MEASURE_SPEED_settings(bpy.types.PropertyGroup):
    """ """
    enabled : BoolProperty(name="Enabled", description="Enabled", default=False)
    frame : IntProperty(name="Frame", description="Frame", default=0)
    speed_prec : IntProperty(name="Precission", description="Speed precission", min=0, default=2)
    font_size : IntProperty(name="Font size", description="Font size", min=0, default=16)
    font_color : FloatVectorProperty(name="Font color", description="Font color", size=4, default=(1.0, 1.0, 1.0, 1.0), subtype='COLOR')
    bg_color : FloatVectorProperty(name="Background color", description="Background color", size=4, default=(0.0, 0.0, 0.0, 1.0), subtype='COLOR')
    line_width : IntProperty(name="Line width", description="Line width", min=0, max=10, default=1)
    label_offset : FloatVectorProperty(name="Label offset", description="Label offset", size=3, default=(0.0, 0.0, 0.0), subtype='TRANSLATION')

    measure_list : CollectionProperty(type=MEASURE_SPEED_measure_settings)
    active_measure_index : IntProperty(name="Active measure index", description="Active measure index", default=0)

bpy.utils.register_class(MEASURE_SPEED_settings)

